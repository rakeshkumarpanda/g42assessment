# G42Assessment
In this project we've various areas. Each and every areas described below

1. src/main/java:
a. helpers: This section is holding a class called FileOperation.java which has the capability to read different files like property and json files.
b. utils: This package is holding a java class called CommonUtils.java where we can store common functions.

2. src/main/resources:
This sections contains 2 different files called config.properties which is holding my base uri and base path to test the apis and it also holds TestData.json which is holding all my desired test data.

3. src/test/java:
a. appTestBase: This section holds a before class annoted method where the basic preconditions like initializing the base uri and base path have done.
b. assessmentTest: This is holding my test case.
c. helpers: This is holding a java file which can trigger my desired endpoints using rest assured library.

Note: Test has been made in such a way that based on the given test data the test should work.

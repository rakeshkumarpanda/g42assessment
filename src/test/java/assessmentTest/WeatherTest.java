package assessmentTest;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import appTestBase.AppTestBase;
import helpers.FileOperation;
import io.restassured.response.Response;
import junit.framework.Assert;

public class WeatherTest extends AppTestBase
{
	
	String jsonFilePath = System.getProperty("user.dir")+"/src/main/resources/TestData.JSON";
	String desiredLocation;
	String woeid;
	
	@Test(description = "This test is to fetch the woeid based on the give city")
	public void getWoeidTest() throws FileNotFoundException, IOException, ParseException
	{
		Map<String, String> searchLocationData = new FileOperation(jsonFilePath).readJson("searchLocation");
		desiredLocation = searchLocationData.get("desiredLocation");
		int expectedStatusCode = Integer.parseInt(searchLocationData.get("statusCode"));
		String id = searchLocationData.get("desiredExtractPath");
		
		String getCallResource = "/search/?query="+desiredLocation;
		
		Response response = apiHelper.getCall(getCallResource);
		int actualStatusCode = apiHelper.getResponseCode(response);
		
		Assert.assertEquals(expectedStatusCode, actualStatusCode);
		
		List<Object> responsePathValue = apiHelper.getResponsePathValue(response, id);
		woeid = responsePathValue.get(0).toString();
	}
	
	@Test(description = "This test is to fetch the forecast details for the desired city", dependsOnMethods = "getWoeidTest")
	public void getForecastDetails() throws FileNotFoundException, IOException, ParseException
	{
		Map<String, String> searchLocationData = new FileOperation(jsonFilePath).readJson("forecastedData");
		int expectedStatusCode = Integer.parseInt(searchLocationData.get("statusCode"));
		String weatherType = searchLocationData.get("weatherType");
		String temp = searchLocationData.get("Temp");
		String wind = searchLocationData.get("Wind");
		String humidity = searchLocationData.get("Humidity");
		
		LocalDate tomorrow = LocalDate.now().plusDays(1);
		String date = tomorrow.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		
		String getCallResource = "/"+woeid+"/"+date+"/";
		
		Response response = apiHelper.getCall(getCallResource);
		int actualStatusCode = apiHelper.getResponseCode(response);
		
		Assert.assertEquals(expectedStatusCode, actualStatusCode);
		
		String weatherTypeValue = apiHelper.getResponsePathValue(response, weatherType).get(0).toString();
		String tempValue = apiHelper.getResponsePathValue(response, temp).get(0).toString();
		String windValue = apiHelper.getResponsePathValue(response, wind).get(0).toString();
		String humidityValue = apiHelper.getResponsePathValue(response, humidity).get(0).toString();
		
		double finalTempValue = commonUtils.formatDecimal(tempValue);
		double finalWindValue = commonUtils.formatDecimal(windValue);
		
		System.out.println("Weather on "+date+" in "+desiredLocation);
		System.out.println(weatherTypeValue+"\nTemp: "+finalTempValue+" �C\nWind: "+finalWindValue+" mph\nHumidity: "+humidityValue+"%");
	}

}

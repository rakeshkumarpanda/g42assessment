package helpers;

import java.util.List;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class ApiHelper 
{
	public Response getCall(String resource)
	{
		Response response = RestAssured.given()
										.when()
										.get(resource);
		return response;
	}
	
	public int getResponseCode(Response response)
	{
		return response.statusCode();
	}
	
	public List<Object> getResponsePathValue(Response response, String path)
	{
		List<Object> pathValues = response.then()
											.extract()
											.path(path);
		return pathValues;
	}

}

package appTestBase;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import org.testng.annotations.BeforeClass;

import helpers.ApiHelper;
import helpers.FileOperation;
import io.restassured.RestAssured;
import utils.CommonUtils;

public class AppTestBase 
{
	public String configFilePath = System.getProperty("user.dir")+"/src/main/resources/config.properties";
	
	public ApiHelper apiHelper;
	public CommonUtils commonUtils;
	
	@BeforeClass
	public void initBase() throws FileNotFoundException, IOException
	{
		Map<String, String> readProperty = new FileOperation(configFilePath).readProperty();
		
		
		RestAssured.baseURI = readProperty.get("baseUri");
		RestAssured.basePath = readProperty.get("basePath");
		
		apiHelper = new ApiHelper();
		commonUtils = new CommonUtils();
	}

}

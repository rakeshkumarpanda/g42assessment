package helpers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class FileOperation 
{
	public JSONParser jsonParser;
	public JSONObject jsonObject;
	public String file;
	
	public FileOperation(String file)
	{
		this.file = file;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> readJson(String jsonNode) throws FileNotFoundException, IOException, ParseException
	{
		jsonParser = new JSONParser();
		jsonObject = (JSONObject)jsonParser.parse(new FileReader(file));
		return (Map<String, String>) jsonObject.get(jsonNode);
	}
	
	public Map<String, String> readProperty() throws FileNotFoundException, IOException
	{
		Properties properties = new Properties();
		properties.load(new FileReader(file));
		Map<String, String> map = new HashMap<String, String>();
		map.putAll(properties.entrySet()
                .stream()
                .collect(Collectors.toMap(e -> e.getKey().toString(), 
                                          e -> e.getValue().toString())));
		return map;
	}

}

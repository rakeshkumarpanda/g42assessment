package utils;

import java.text.DecimalFormat;

public class CommonUtils 
{
	public double formatDecimal(String value)
	{
		double d = Double.parseDouble(value);
		DecimalFormat decimalFormat = new DecimalFormat("##.#");
		return Double.parseDouble(decimalFormat.format(d));
	}

}
